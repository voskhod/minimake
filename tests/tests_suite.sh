#!/bin/sh

YELLOW='\033[0;33m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'

./minimake -f tests/Makefile.syntax-test -p > out 2> /dev/null
 diff "out" "tests/ref_syntax" > test_output
if [ "$?" -eq 1 ]; then
    echo -e "[${RED}FAILED${NC}] Makefile.syntax_test"
    rm out
    exit 1
else
    echo -e "[${GREEN}PASSED${NC}] Makefile.syntax_test"
fi


nb=0
for m in tests/makefile_test_suite/Makefile.*; do
    ./minimake -f "$m" > out 2> err

    diff "out" "tests/ref_stdout/ref-$nb" > test_output
    if [ "$?" -eq 1 ]; then
        echo -e "[${RED}FAILED${NC}] $m" | sed "s/\].*\//] /" 
        rm out err
        exit 1
    else
        echo -e "[${GREEN}PASSED${NC}] $m" | sed "s/\].*\//] /"
    fi

    nb=$(($nb + 1))
done

echo ""
echo "Everything is ok"
rm out err test_output
exit 0
