#include <criterion/criterion.h>
#include <string.h>
#include "../src/stack.h"
#include "../src/parser.h"
#include "../src/expand.h"


struct var *var_create(char *name, char *value)
{
    struct var *var = var_init(name);
    var->value = value;

    return var;
}

Test(Find_var, Existing_var)
{
    struct stack *var = NULL;
    char var1[] = "name_a";
    char var2[] = "name_b";
    char var3[] = "name_c";

    stack_var_push(&var, var_create(var1, "value_a"));
    stack_var_push(&var, var_create(var2, "value_b"));
    stack_var_push(&var, var_create(var3, "value_c"));

    char target[] = "name_a";
    cr_assert(!strcmp(find_var(var, target, 6), "value_a"));
}


Test(Find_var, Existing_var_end)
{
    struct stack *var = NULL;
    stack_var_push(&var, var_create("name_a", "value_a"));
    stack_var_push(&var, var_create("name_b", "value_b"));
    stack_var_push(&var, var_create("name_c", "value_c"));

    cr_assert(!strcmp(find_var(var, "name_c", 6), "value_c"));
}


Test(Find_var, Non_existing_var)
{
    struct stack *var = NULL;
    stack_var_push(&var, var_create("name_a", "value_a"));
    stack_var_push(&var, var_create("name_b", "value_b"));
    stack_var_push(&var, var_create("name_c", "value_c"));

    cr_assert(!strcmp(find_var(var, "name_k", 6), ""));
}


Test(Find_var, Empty_stack)
{
    struct stack *var = NULL;

    cr_assert(!strcmp(find_var(var, "name_a", 6), ""));
}


Test(Find_var, One_char)
{
    struct stack *var = NULL;
    stack_var_push(&var, var_create("a", "value_a"));
    stack_var_push(&var, var_create("name_b", "value_b"));
    stack_var_push(&var, var_create("name_c", "value_c"));

    cr_assert(!strcmp(find_var(var, "a", 1), "value_a"));
}

Test(Expand, Expand_two)
{
    struct stack *var = NULL;
    stack_var_push(&var, var_create("name_a", "value_a"));
    stack_var_push(&var, var_create("name_b", "value_b"));
    stack_var_push(&var, var_create("name_c", "value_c"));

    char *in = "qwe $(name_a) ${name_b} qwe";
    char *out = expand(var, in, strlen(in));
    cr_assert(!strcmp(out, "qwe value_a value_b qwe"));
    free(out);
}


Test(Expand, Expand_var_one_char)
{
    struct stack *var = NULL;
    stack_var_push(&var, var_create("a", "value_a"));
    stack_var_push(&var, var_create("name_b", "value_b"));
    stack_var_push(&var, var_create("name_c", "value_c"));

    char *in = "qwe $a ${name_b} qwe";
    char *out = expand(var, in, strlen(in));
    cr_assert(!strcmp(out, "qwe value_a value_b qwe"));
    free(out);
}


Test(Expand, Unknown_var)
{
    struct stack *var = NULL;

    char *in = "qwe $a qwe";
    char *out = expand(var, in, strlen(in));
    cr_assert(!strcmp(out, "qwe  qwe"));
    free(out);
}

