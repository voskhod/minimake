#include <criterion/criterion.h>
#include <string.h>

#include "../src/better_string.h"


Test(String, Init)
{
    string_destroy(string_init());
}


Test(String, Cat)
{
    char *str1 = "aze";
    char *str2 = "sfg";

    char *expected = calloc(1024, sizeof(char));
    expected = strcpy(expected, str1);
    expected = strcat(expected, str2);

    struct string *str = string_init();
    string_ncat(str, str1, strlen(str1));
    string_ncat(str, str2, strlen(str2));

    cr_assert(strcmp(str->str, expected) == 0);
}
