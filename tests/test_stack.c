#include <criterion/criterion.h>
#include "../src/stack.h"


Test(Stack_push, Empty)
{
    struct stack *s = NULL;
    struct var v = { "a", NULL };

    stack_var_push(&s, &v);

    cr_assert(s);
}


Test(Stack_Push, Multi)
{
    struct stack *s = NULL;
    struct var v1 = { "a", NULL };
    struct var v2 = { "b", NULL };

    stack_var_push(&s, &v1);
    stack_var_push(&s, &v2);

    cr_assert_eq(s->data.var->name[0], 'b');
    cr_assert_eq(s->next->data.var->name[0], 'a');
}


Test(Stack_Find, Valid)
{
    struct stack *s = NULL;
    struct var v1 = { "a", NULL };
    struct var v2 = { "b", NULL };
    stack_var_push(&s, &v1);
    stack_var_push(&s, &v2);

    struct var *f = stack_var_find(&s, "a");
    cr_assert_eq(f->name[0], 'a');

    f = stack_var_find(&s, "b");
    cr_assert_eq(f->name[0], 'b');
}

Test(Stack_Find, Invalid)
{
    struct stack *s = NULL;
    struct var v1 = { "a", NULL };
    struct var v2 = { "b", NULL };
    stack_var_push(&s, &v1);
    stack_var_push(&s, &v2);

    struct var *f = stack_var_find(&s, "c");
    cr_assert_eq(f, NULL);
}


Test(Stack_Find, Empty)
{
    struct stack *s = NULL;

    struct var *f = stack_var_find(&s, "s");
    cr_assert_eq(f, NULL);
}

