#include <criterion/criterion.h>
#include "../src/vector.h"

Test(Vector, Init_and_destroy)
{
    struct vector_ref *v = vector_init(5);

    cr_assert(v);
    vector_destroy(v);
}


Test(Vector, Push_one)
{
    struct vector_ref *v = vector_init(5);

    void *p = malloc(1);
    vector_push(v, p);
    cr_assert_eq(v->data[0], p);

    vector_destroy(v);
}


Test(Vector, Push_Many)
{
    struct vector_ref *v = vector_init(1);

    void *p1 = malloc(1);
    vector_push(v, p1);
    cr_assert_eq(v->data[0], p1);

    void *p2 = malloc(1);
    vector_push(v, p2);
    cr_assert_eq(v->data[1], p2);

    void *p3 = malloc(1);
    vector_push(v, p3);
    cr_assert_eq(v->data[2], p3);


    vector_destroy(v);
}
