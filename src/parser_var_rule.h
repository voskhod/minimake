#ifndef PARSER_VAR_RULE_H
#define PARSER_VAR_RULE_H

char *get_var(char *input, char **result, int *size);

char *get_recipe(char *input);

char *get_next_dep(char *input, char **result, int *size);

#endif /* PARSER_VAR_RULE_H */
