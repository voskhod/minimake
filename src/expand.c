#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "stack.h"
#include "utilities.h"
#include "str_manipulation.h"
#include "parser_var_rule.h"


char *find_var(struct stack *stack_var, char *var, int size)
{
    for (; stack_var != NULL; stack_var = stack_var->next)
    {
        if (!strncmp(stack_var->data.var->name, var, size))
        {
            char *result = NULL;
            int n;
            get_var(stack_var->data.var->value, &result, &n);
            result[n] = '\0';
            return result;
        }
    }

    char *str = strndup(var, size);
    char *var_env = getenv(str);
    free(str);

    if (var_env != NULL)
        return var_env;

    return "";
}


static int is_not_dollar(int c)
{
    return (is_not_newline_com(c) && c != '$');
}

static int is_not_closing(int c)
{
    return (is_not_newline_com(c) && c != ')' && c != '}');
}


static int find_expanded_var(char *var, int size, int *begin, int *end)
{
    *begin = skip_class_n(is_not_dollar, var, size);

    if (var[*begin] == '$')
    {
        if (var[*begin + 1] == '(' || var[*begin + 1] == '{')
        {
            *begin += 2;
            *end = skip_class_n(is_not_closing, var, size);

            if (var[*end] == '\n' || var[*end] == '\0')
                errx(2, "Missing ')' or '}'");

            return 2;
        }
        else
        {
            *begin += 1;
            *end = *begin;
            return 1;
        }
    }

    return 0;
}


char *expand(struct stack *stack_var, char *var, int size)
{
    char *output = xcalloc(1024, sizeof(char));

    int begin = 0;
    int end = 0;
    int offset = find_expanded_var(var, size, &begin, &end);

    if (offset)
    {
        strncpy(output, var, begin - offset);

        if (var[begin] == '$')
            output[begin - offset] = '$';
        else
        {
            char *exp = find_var(stack_var, var + begin,
                                    end - begin + 2 - offset);
            strcat(output, exp);
        }
        char *end_line = expand(stack_var, var + end + 1, size - end - 1);
        strcat(output, end_line);
        free(end_line);
    }
    else
        strncpy(output, var, size);

    return output;
}
