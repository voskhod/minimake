#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "stack.h"
#include "expand.h"
#include "execute.h"
#include "utilities.h"
#include "parser_var_rule.h"
#include "str_manipulation.h"


void display_status(char *target, int status)
{
    if (status == NOTHING_TO_DO)
        printf("minimake: Nothing to be done for '%s'.\n", target);
    else if (status == UP_TO_DATE)
        printf("minimake: '%s' is up to date.\n", target);
}


static int execute_dep(char *dep, struct stack *rule, struct stack *var)
{
    char *next_dep = NULL;
    int n = 0;
    int status = UP_TO_DATE;

    char *str = get_next_dep(dep, &next_dep, &n);
    char *ptr = NULL;
    while (n != 0)
    {
        ptr = strndup(next_dep, n);
        int cur_status = execute(rule, var, ptr, rule);
        status = (cur_status > status) ? status : cur_status;

        str = get_next_dep(str, &next_dep, &n);
        free(ptr);
    }

    return status;
}


int execute_recipe(struct list *recipes, struct stack *var)
{
    if (recipes == NULL)
        return NOTHING_TO_DO;

    execute_recipe(recipes->next, var);

    char *cmd = recipes->data;
    cmd += skip_class(iswhite, cmd);
    cmd = expand(var, cmd, strlen(cmd));

    if (cmd[0] == '@')
        cmd[0] = ' ';
    else
        printf("%s", cmd);

    fflush(stdout);

    pid_t pid = fork();
    if (pid < 0)
        err(2, "Unable to fork");

    if (pid == 0)
    {
        execlp("/bin/sh", "sh", "-c", cmd, NULL);
        err(2, "Unable to lauch /bin/sh");
    }

    int wstatus;
    waitpid(pid, &wstatus, 0);
    if(WEXITSTATUS(wstatus) != 0)
        errx(2, "Error while executing");
    free(cmd);

    return UPDATE;
}


static int execute_scheduling(struct rule *rule, struct stack *var,
                                struct stack *rule_ref)
{
    int status_dep;

    if (rule == NULL || rule->already_execute)
        return UP_TO_DATE;

    rule->already_execute = 1;

    status_dep = execute_dep(rule->dep, rule_ref, var);

    int status_recipie = UP_TO_DATE;
    status_recipie = execute_recipe(rule->recipe, var);

    return (status_dep > status_recipie) ? status_recipie : status_dep;
}


int execute(struct stack *rule_cur, struct stack *var,
            char *target, struct stack *rule_ref)
{
    if (rule_cur == NULL)
        errx(2, "No rule to make target '%s'.", target);

    if (!strcmp(rule_cur->data.rule->target, target))
        return execute_scheduling(rule_cur->data.rule, var, rule_ref);
    else
        return execute(rule_cur->next, var, target, rule_ref);
}


void execute_last(struct stack *rule_cur, struct stack *var,
                    struct stack *rule_ref)
{
    if (rule_cur == NULL)
        errx(2, "No rule found");

    if (rule_cur->next != NULL)
    {
        execute_last(rule_cur->next, var, rule_ref);
        return;
    }
    int status = execute(rule_cur, var, rule_cur->data.rule->target, rule_ref);

    display_status(rule_cur->data.rule->target, status);
}
