#ifndef VECTOR_H
#define VECTOR_H

#include <stddef.h>

struct vector_ref
{
    void **data;
    size_t size;
    size_t capacity;
};

struct vector_ref *vector_init(size_t size);

void vector_push(struct vector_ref *vector, void *ref);

void vector_destroy(struct vector_ref *vector);

#endif /* VECTOR_H */
