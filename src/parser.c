#define _GNU_SOURCE
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "stack.h"
#include "vector.h"
#include "expand.h"
#include "var_rule.h"
#include "utilities.h"
#include "str_manipulation.h"


static enum type get_type(char c)
{
    if (c == '=')
        return MAKE_VAR;

    else if (c == ':')
        return MAKE_RULE;

    else if (c == '#')
        return MAKE_COMMENT;

    else if (c == '\t')
        return MAKE_RECIPE;

    return MAKE_NONE;
}


char *get_field(char *str, char **field, int *n)
{
    int i = skip_class(isspace, str);
    str = str + i;
    *field = str;

    *n = skip_class(isvalid, str);

    if (*n == 0)
        return str;

    str += *n;

    str += skip_class(isspace, str);

    return str;
}


static struct var *get_var(char *name, char *value)
{
    struct var *var = var_init(name);
    var->value = value;

    return var;
}


static struct rule *get_rule(struct stack *stack_var, char *name, char *dep)
{
    struct rule *rule = rule_init(name);

    rule->dep = expand(stack_var, dep, strlen(dep));

    return rule;
}


void parse_line(char *line, struct stack **var, struct stack **rule)
{
    enum type type = get_type(line[0]);
    if (type == MAKE_COMMENT)
        return;

    else if (type == MAKE_RECIPE)
    {
        char c = line[skip_class(iswhite, line)];
        if (c == '\n' || c == '\0')
            return;

        if ((*rule) == NULL)
        {
            parse_line(line + skip_class(iswhite, line), var, rule);
            return;
        }

        stack_push_recipe(rule, line);
        return;
    }

    char *name = NULL;
    int n = 0;
    line = get_field(line, &name, &n);
    if (n == 0)
        return;

    type = get_type(line[0]);
    name[n] = '\0';

    if (type == MAKE_VAR)
        stack_var_push(var, get_var(name, line + 1));

    else if (type == MAKE_RULE)
        stack_rule_push(rule, get_rule(*var, name, line + 1));

    else if (type == MAKE_NONE)
        errx(2, "Error syntax");
}



struct vector_ref *parser(FILE *input, struct stack **stack_var,
                            struct stack **stack_rule)
{
    char *line = NULL;
    size_t n = 0;
    ssize_t r = 0;

    struct vector_ref *refs = vector_init(50);

    for (;;)
    {
        line = NULL;
        r = getline(&line, &n, input);
        vector_push(refs, line);

        if (r <= 0)
            break;

        parse_line(line, stack_var, stack_rule);
    }

    return refs;
}

