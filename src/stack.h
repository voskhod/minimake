#ifndef STACK_H
#define STACK_H

#include "var_rule.h"


union data
{
    struct var *var;
    struct rule *rule;
};


struct stack
{
    struct stack *next;
    union data data;
};


void stack_var_push(struct stack **stack, struct var *var);
struct var *stack_var_find(struct stack **stack, char *name);
void stack_var_destroy(struct stack **stack);

void stack_rule_push(struct stack **stack, struct rule* rule);
struct rule *stack_rule_find(struct stack **stack, char *name);
void stack_rule_destroy(struct stack **stack);

void stack_push_recipe(struct stack **rule, char *line);

#endif /* STACK_H */
