#include <string.h>
#include <stdlib.h>
#include "var_rule.h"
#include "utilities.h"

struct var *var_init(char *name)
{
    struct var *var = xcalloc(1, sizeof(struct var));
    var->name = name;

    return var;
}


struct rule *rule_init(char *target)
{
    struct rule *rule = xcalloc(1, sizeof(struct rule));
    rule->target = target;
    rule->already_execute = 0;
    return rule;
}


void list_destroy(struct list *l)
{
    if (l == NULL)
        return;

    list_destroy(l->next);
    free(l);
}

