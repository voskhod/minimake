#ifndef STR_MANIPULATION_H
#define STR_MANIPULATION_H

int iswhite(int c);

int is_not_newline_com(int c);

int is_not_newline(int c);

int isvalid(int c);

int skip_class(int (*classifier)(int c), char *cursor);

int skip_class_n(int (*classifier)(int c), char *cursor, int n);

#endif /* STR_MANIPULATION_H */
