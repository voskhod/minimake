#include <stdlib.h>
#include "vector.h"
#include "utilities.h"


struct vector_ref *vector_init(size_t size)
{
    struct vector_ref *vector = xcalloc(1, sizeof(struct vector_ref));

    vector->capacity = size;
    vector->data = xcalloc(vector->capacity, sizeof(void *));

    return vector;
}


void vector_push(struct vector_ref *vector, void *ref)
{
    if (vector->size >= vector->capacity)
    {
        vector->capacity = vector->capacity * 2;
        vector->data = realloc(vector->data,
                                vector->capacity * sizeof(void *));
    }

    vector->data[vector->size] = ref;
    vector->size++;
}


void vector_destroy(struct vector_ref *vector)
{
    for (size_t i = 0; i < vector->size; i++)
        free(vector->data[i]);

    free(vector->data);
    free(vector);
}
