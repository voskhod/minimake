#ifndef BETTER_STRING
#define BETTER_STRING

struct string
{
    char *str;
    unsigned capacity;
    unsigned size;
};

struct string *string_init();
void string_destroy(struct string *str);

struct string *string_ncat(struct string *dst, char *src, unsigned n);

#endif /* BETTER_STRING */
