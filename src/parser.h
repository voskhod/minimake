#ifndef PARSER_H
#define PARSER_H

#include <stdio.h>
#include "stack.h"
#include "vector.h"

char *get_field(char *str, char **field, int *n);


struct vector_ref *parser(FILE *input, struct stack **stack_var,
                            struct stack **stack_rule);

#endif /* PARSER_H */
