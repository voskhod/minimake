#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <err.h>
#include "stack.h"
#include "vector.h"
#include "parser.h"
#include "execute.h"
#include "pretty_print.h"

void print_help(void)
{
    printf("Usage: miniake [options] [target] ...\n");
    printf("Options:\n");
    printf("  -f FILE  Read FILE as a minimakefile.\n");
    printf("  -h       Display help.\n");
    printf("  -p       Display all variables and all rules.\n");
}


void execute_makefile(FILE *makefile, int print, char *target[], int nb_target)
{
    struct stack *rule = NULL;
    struct stack *var = NULL;

    struct vector_ref *refs = parser(makefile, &var, &rule);

    if (print)
    {
        pretty_print(var, rule);
        return;
    }

    if (nb_target > 0)
    {
        for (int i = 0; i < nb_target; i++)
        {
            int status = execute(rule, var, target[i], rule);
            display_status(target[i], status);
        }
    }
    else
    {
        if (rule != NULL)
            execute_last(rule, var, rule);
    }

    stack_var_destroy(&var);
    stack_rule_destroy(&rule);

    vector_destroy(refs);

    fclose(makefile);
}


FILE *get_file(char *file_name)
{
    FILE *file;

    if (file_name != NULL)
    {
        file = fopen(file_name, "r");

        if (file == NULL)
            errx(2, "No makefile found");
    }
    else
    {
        file = fopen("minimakefile", "r");

        if (file == NULL)
            file = fopen("Minimakefile", "r");

        if (file == NULL)
            errx(2, "No minmakefile found");
    }

    return file;
}


int main(int argc, char *argv[])
{
    char *file_name = NULL;
    int print = 0;
    int help = 0;
    int opt;

    while ((opt = getopt(argc, argv, "hpf:")) != -1)
    {
        switch (opt)
        {
        case 'p':
            print = 1;
            break;

        case 'h':
            help = 1;
            break;

        case 'f':
            file_name = optarg;
            break;

        default:
            errx(2, "Invalid args\nTry minimake --help for more information.");
        }
    }

    if (help)
    {
        print_help();
        return 0;
    }

    FILE *file = get_file(file_name);

    execute_makefile(file, print, argv + optind, argc - optind);

    return 0;
}
