#include <stdlib.h>
#include <string.h>

#include "utilities.h"
#include "better_string.h"


struct string *string_init()
{
    struct string *str = xcalloc(1, sizeof(struct string));
    return str;
}


void string_destroy(struct string *str)
{
    free(str->str);
    free(str);
}


struct string *string_ncat(struct string *dst, char *src, unsigned n)
{
    unsigned new_size = dst->size +strlen(src);

    if (dst->capacity <= new_size)
    {
        dst->capacity += 100;
        dst->str = realloc(dst->str, dst->capacity);
    }

    strncpy(dst->str + dst->size, src, n);
    dst->size = new_size;

    return dst;
}
