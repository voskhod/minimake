#ifndef UTILITIES_H
#define UTILITIES_H

#include <err.h>
#include <stddef.h>

void *xmalloc(size_t size);

void *xcalloc(size_t nmemb, size_t size);

#endif /* UTILITIES_H */
