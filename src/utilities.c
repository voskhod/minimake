#include <stdlib.h>
#include "utilities.h"

void *xmalloc(size_t size)
{
    void *ptr = malloc(size);
    if (ptr == NULL)
        err(2, "Fail to allocate memory");

    return ptr;
}


void *xcalloc(size_t nmemb, size_t size)
{
    void *ptr = calloc(nmemb, size);
    if (ptr == NULL)
        err(2, "Fail to allocate memory");

    return ptr;
}
