#ifndef VAR_RULE_H
#define VAR_RULE_H


enum type
{
    MAKE_VAR = 0,
    MAKE_RULE,
    MAKE_COMMENT,
    MAKE_RECIPE,
    MAKE_NONE
};


struct var
{
    char *name;
    char *value;
};


struct list
{
    struct list *next;
    char *data;
};


struct rule
{
    char *target;
    char *dep;
    struct list *recipe;
    int already_execute;
};


struct var *var_init(char *name);

struct rule *rule_init(char *target);

void list_destroy(struct list *l);

#endif /* VAR_RULE_H */
