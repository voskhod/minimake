#include <ctype.h>
#include "parser.h"
#include "str_manipulation.h"


char *get_var(char *input, char **result, int *size)
{
    input += skip_class(isspace, input);
    *result = input;

    *size = skip_class(is_not_newline_com, input);

    return input + *size;
}


char *get_recipe(char *input)
{
    return input + skip_class(isspace, input);
}


char *get_next_dep(char *input, char **result, int *size)
{
    return get_field(input, result, size);
}

