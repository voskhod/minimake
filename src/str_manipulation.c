#include <ctype.h>
#include "str_manipulation.h"


int iswhite(int c)
{
    return (c != '\0' && (c == ' ' || c == '\t'));
}


int is_not_expand(int c)
{
    return (c != '\0' && c != '\n' && c != '$');
}


int not_closing(int c)
{
    return (c != '\0' && c != '\n' && c != ')' && c != '}');
}

int isvalid(int c)
{
    return (c != '\0' && !isspace(c) && c != ':' && c != '=' && c != '#');
}


int is_not_newline_com(int c)
{
    return (c != '\0' && c != '#' && c != '\n');
}


int is_not_newline(int c)
{
    return (c != '\0' && c != '\n');
}


int skip_class(int (*classifier)(int c), char *cursor)
{
    int i = 0;
    while (classifier(cursor[i]))
        i++;

    return i;
}


int skip_class_n(int (*classifier)(int c), char *cursor, int n)
{
    int i = 0;
    while (classifier(cursor[i]) && i < n)
        i++;

    return i;
}

