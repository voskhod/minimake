#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "stack.h"
#include "expand.h"
#include "parser.h"
#include "var_rule.h"
#include "parser_var_rule.h"
#include "str_manipulation.h"


static void print_var(struct stack *stack_var)
{
    if (stack_var == NULL)
        return;
    print_var(stack_var->next);

    struct var *v = stack_var->data.var;

    char *value = NULL;
    int size;
    get_var(v->value, &value, &size);
    value = expand(stack_var, value, size);
    get_var(value, &value, &size);

    printf("'%s' = '%.*s'\n", v->name, size, value);

    free(value);
}


static void print_recipe(struct list *recipe)
{
    if (recipe == NULL)
        return;
    print_recipe(recipe->next);

    char *str = get_recipe(recipe->data);
    printf("\t'%.*s'\n", skip_class(is_not_newline, str), str);
}


static void print_rule(struct stack *stack_rule)
{
    if (stack_rule == NULL)
        return;
    print_rule(stack_rule->next);

    struct rule *r = stack_rule->data.rule;

    printf("(%s):", r->target);

    char *value = NULL;
    int n;

    char *str = get_next_dep(r->dep, &value, &n);
    while (n != 0)
    {
        printf(" [%.*s]", n, value);
        str = get_next_dep(str, &value, &n);
    }

    printf("\n");
    print_recipe(r->recipe);
}

void pretty_print(struct stack *var, struct stack *rule)
{
    printf("# variables\n");
    print_var(var);
    printf("# rules\n");
    print_rule(rule);
}

