#ifndef EXPAND_H
#define EXPAND_H

char *find_var(struct stack *stack_var, char *var, int size);

char *expand(struct stack *stack_var, char *var, int size);

#endif /* EXPAND_H */
