#ifndef EXECUTE_H
#define EXECUTE_H

#define UPDATE 0
#define NOTHING_TO_DO 1
#define UP_TO_DATE 2

void display_status(char *target, int status);

int execute(struct stack *rule, struct stack *var,
                char *target, struct stack *rule_ref);

void execute_last(struct stack *rule_cur, struct stack *var,
                    struct stack *rule_ref);

#endif /* EXECUTE_H */
