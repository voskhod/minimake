#include <stdlib.h>
#include <string.h>
#include "utilities.h"
#include "stack.h"
#include "var_rule.h"


void push(struct stack **stack, union data data)
{
    struct stack *new = xcalloc(1, sizeof(struct stack));
    new->data = data;
    new->next = *stack;
    *stack = new;
}


static void destroy(struct stack *stack, enum type type)
{
    if (stack == NULL)
        return;
    destroy(stack->next, type);

    if (type == MAKE_VAR)
        free(stack->data.var);
    else
    {
        list_destroy(stack->data.rule->recipe);
        free(stack->data.rule->dep);
        free(stack->data.rule);
    }
    free(stack);
}


void stack_var_push(struct stack **stack, struct var *var)
{
    union data data;
    data.var = var;
    push(stack, data);
}


struct var *stack_var_find(struct stack **stack, char *name)
{
    struct stack *s = *stack;
    for (; s != NULL; s = s->next)
        if (!strcmp(s->data.var->name, name))
            return s->data.var;

    return NULL;
}


void stack_var_destroy(struct stack **stack)
{
    destroy(*stack, MAKE_VAR);
}


void stack_rule_push(struct stack **stack, struct rule *rule)
{
    union data data;
    data.rule = rule;
    push(stack, data);
}


struct rule *stack_rule_find(struct stack **stack, char *name)
{
    struct stack *s = *stack;
    for (; s != NULL; s = s->next)
        if (!strcmp(s->data.rule->target, name))
            return s->data.rule;

    return NULL;
}


void stack_rule_destroy(struct stack **stack)
{
    destroy(*stack, MAKE_RULE);
}


void stack_push_recipe(struct stack **rule, char *line)
{
    struct stack *r = *rule;
    struct list *new = xcalloc(1, sizeof(struct list));
    new->data = line;
    new->next = r->data.rule->recipe;
    r->data.rule->recipe = new;
}
