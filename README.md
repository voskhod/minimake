# Minimake

Minimake is a simple version of make but instead of Makefile, it takes
a minimakefile.

# Build
`make`

# Usage
`./minimake` in a directory with a Minimakefile or `./Minimake -f PATH`.
