CC=gcc
CPPFLAGS=-D=_DEFAULT_SOURCE
CFLAGS=-Wall -Wextra -Werror -std=c99 -pedantic -g

SRC= vector.c execute.c expand.c str_manipulation.c stack.c utilities.c var_rule.c pretty_print.c minimake.c parser.c parser_var_rule.c
OBJ=$(addprefix src/,${SRC:.c=.o})

TESTS=test_vector test_stack
TEST_LDLIBS= -lcriterion

.PHONY: all clean check

all: minimake
	gcc $(OBJ) -o $^

minimake: $(OBJ)

check: CC+=-fsanitize=address
check: clean $(TESTS)
#	for t in $(TESTS); do \
#		./$$t; \
#	done;
	./tests/tests_suite.sh

# test_%: tests/test_%.o src/%.o utilities.o
# 	$(CC) $(LDFLAGS) -o $@ $^ $(TEST_LDLIBS)

test_expand: tests/test_expand.o src/parser_var_rule.o src/str_manipulation.o src/stack.o src/utilities.o src/var_rule.o src/parser.o src/expand.o
	$(CC) $(LDFLAGS) -o $@ $^ $(TEST_LDLIBS)

test_better_string: tests/test_better_string.o src/utilities.o src/better_string.o
	$(CC) $(LDFLAGS) -o $@ $^ $(TEST_LDLIBS)

test_stack: tests/test_stack.o src/stack.o src/utilities.o src/var_rule.o
	$(CC) $(LDFLAGS) -o $@ $^ $(TEST_LDLIBS)

test_vector: tests/test_vector.o src/vector.o src/utilities.o
	$(CC) $(LDFLAGS) -o $@ $^ $(TEST_LDLIBS)


clean:
	rm -f $(OBJ) $(TESTS)
	rm -f tests/*.o src/*.o
